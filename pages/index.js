import Bottom from '../components/Bottom/Bottom';
import Hero from '../components/Hero/Hero';
import Head from 'next/head';

export default function Home() {
  return (
    <>
      <Head>
        <title>Binar Car Rental</title>
      </Head>

      <Hero />
      <Bottom />
    </>
  )
}
