import { Provider } from 'react-redux';
import { globalStore, persistor } from '../config/redux/store';
import { PersistGate } from 'redux-persist/integration/react';
import '../styles/globals.css';

function MyApp({ Component, pageProps }) {
  return (
    <Provider store={globalStore}>
      <PersistGate persistor={persistor}>
        <Component {...pageProps} />
      </PersistGate>
    </Provider>
  );
};

export default MyApp;
