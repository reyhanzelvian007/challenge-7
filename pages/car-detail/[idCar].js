import BottomDetail from "../../components/BottomDetail/BottomDetail";
import Top from "../../components/Top/Top";
import Head from "next/head";

const DetailMobil = () => {
  return (
    <>
      <Head>
        <title>Detail Mobil</title>
      </Head>

      <Top />
      <BottomDetail />
    </>
  )
};

export default DetailMobil;