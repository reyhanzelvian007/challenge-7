import TopInvoice from '../../components/TopInvoice/TopInvoice';
import BottomInvoice from '../../components/BottomInvoice/BottomInvoice';
import Head from 'next/head';

const Invoice = () => {
    return (
        <>
            <Head>
                <title>Invoice</title>
            </Head>
        
            <TopInvoice />
            <BottomInvoice />
        </>
    )
};

export default Invoice;