import React from 'react';
import BottomSearch from '../../components/BottomSearch/BottomSearch';
import Top from '../../components/Top/Top';
import Head from 'next/head';

const HasilPencarian = () => {
  return (
    <>
      <Head>
        <title>Hasil Pencarian</title>
      </Head>

      <Top />
      <BottomSearch />
    </>
  )
}

export default HasilPencarian;