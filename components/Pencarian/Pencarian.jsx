import style from './Pencarian.module.css';

const Pencarian = () => {
    return (
        <>
            <div className={style.cariMobilContainer}>
                <div>
                    <h6>Pencarianmu</h6>
                </div>
                <div className={style.cariMobil}>
                    <div className={style.poinCariMobil}>
                        <div>
                            <p>Tipe Driver</p>
                        </div>
                        <div className={style.tipeDriver}>
                            <select name="tipeDriver" id="tipeDriver" placeholder="Pilih Tipe Driver">
                                <option value="sopir">Dengan Sopir</option>
                                <option value="tanpa-sopir">Tanpa Sopir (Lepas Kunci)</option>
                            </select>
                        </div>
                    </div>
                    <div className={style.poinCariMobil}>
                        <div>
                            <p>Tanggal</p>
                        </div>
                        <div className={style.date}>
                            <input id="date" type="date"/>
                        </div>
                    </div>
                    <div className={style.poinCariMobil}>
                        <div>
                            <p>Waktu Jemput/Ambil</p>
                        </div>
                        <div className={style.time}>
                            <input type="time" />
                        </div>
                    </div>
                    <div className={style.poinCariMobil}>
                        <div>
                            <p>Jumlah Penumpang (Optional)</p>
                        </div>
                        <div className={style.penumpang}>
                            <input placeholder="Jumlah Penumpang" type="number" />
                        </div>
                    </div>
                    <div>
                        <button className={style.buttonCariMobil}>Edit</button>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Pencarian;