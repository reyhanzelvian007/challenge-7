import usersImage from '../../public/fi_users.svg';
import settingImage from '../../public/fi_settings.svg';
import calendarImage from '../../public/fi_calendar.svg';
import style from './Card.module.css';
import Link from 'next/link';
import { useSelector } from 'react-redux';
import Image from 'next/image';

const Card = () => {

    const { dataCar } = useSelector((globalStore) => globalStore.carDataReducer);

    return (
        <>
            {dataCar.map((data) => (
                <div key={data.id} className={style.cardContainer}>
                    <div className={style.cardInside}>
                        <div className={style.cardImage}>
                            <img src={data.image} alt="car-image" />
                        </div>
                        <div className={style.cardDesc}>
                            <h6>{data.name}</h6>
                            <h5>Rp {data.price} / hari</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                        <div className={style.cardPoint}>
                            <Image src={usersImage} alt="users-image" />
                            <p>4 orang</p>
                        </div>
                        <div className={style.cardPoint}>
                            <Image src={settingImage} alt="setting-image" />
                            <p>Manual</p>
                        </div>
                        <div className={style.cardPoint}>
                            <Image src={calendarImage} alt="calendar-image" />
                            <p>Tahun 2020</p>
                        </div>
                    </div>
                    <div className={style.buttonContainer}>
                        <Link href={`/car-detail/${data.id}`}>
                            <button>Pilih Mobil</button>
                        </Link>
                    </div>
                </div>
            ))}
        </>
  )
}


export default Card;