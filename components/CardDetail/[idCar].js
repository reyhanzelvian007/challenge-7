import style from './CardDetail.module.css';
import user_image from '../../public/fi_users_detail.svg';
import setting_image from '../../public/fi_settings_detail.svg';
import calendar_image from '../../public/fi_calendar_detail.svg';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import Image from 'next/image';
import Link from 'next/link';

const CardDetail = () => {
    const router = useRouter();

    const { idCar } = router.query;

    const { dataCar } = useSelector((globalStore) => globalStore.carDataReducer);

    let index = -1;

    for (let i=0; i < dataCar.length; i++){
        if (dataCar[i].id == idCar) {
            index = i;
        }
    }

  return (
    <>
        <div className={style.container}>
            <div className={style.imgContainer}>
                <img src={dataCar[index]?.image} alt="car-detail" />
            </div>
            <div className={style.cardDesc}>
                <div>
                    <h6>{dataCar[index]?.name}</h6>
                </div>
                <div className={style.cardPointContainer}>
                    <div className={style.cardPoint}>
                        <Image src={user_image} alt="user-image" />
                        <p>4 orang</p>
                    </div>
                    <div className={style.cardPoint}>
                        <Image src={setting_image} alt="setting-image" />
                        <p>Manual</p>
                    </div>
                    <div className={style.cardPoint}>
                        <Image src={calendar_image} alt="calendar-image" />
                        <p>Tahun 2020</p>
                    </div>
                </div>
            </div>
            <div className={style.total}>
                <div>
                    <p>Total</p>
                </div>
                <div>
                    <h6>Rp {dataCar[index]?.price}</h6>
                </div>
            </div>
            <div>
                <Link href={'/invoice'}>
                    <button>Lanjutkan Pembayaran</button>
                </Link>
            </div>
        </div>
    </>
  )
}

export default CardDetail;