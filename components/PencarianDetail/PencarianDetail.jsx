import style from './PencarianDetail.module.css';

const PencarianDetail = () => {
  return (
    <>
        <div className={style.cariMobilContainer}>
                <div>
                    <h6>Pencarianmu</h6>
                </div>
                <div className={style.cariMobil}>
                    <div className={style.poinCariMobil}>
                        <div>
                            <p>Tipe Driver</p>
                        </div>
                        <div>
                            <select name="tipeDriver" id="tipeDriver" placeholder="Pilih Tipe Driver">
                                <option value="sopir">Dengan Sopir</option>
                                <option value="tanpa-sopir">Tanpa Sopir (Lepas Kunci)</option>
                            </select>
                        </div>
                    </div>
                    <div className={style.poinCariMobil}>
                        <div>
                            <p>Tanggal</p>
                        </div>
                        <div>
                            
                        </div>
                    </div>
                    <div className={style.poinCariMobil}>
                        <div>
                            <p>Waktu Jemput/Ambil</p>
                        </div>
                        <div></div>
                    </div>
                    <div className={style.poinCariMobil}>
                        <div>
                            <p>Jumlah Penumpang (optional)</p>
                        </div>
                        <div></div>
                    </div>
                </div>
            </div>
    </>
  )
}

export default PencarianDetail;