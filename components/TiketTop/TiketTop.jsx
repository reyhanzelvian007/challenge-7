import style from './TiketTop.module.css';
import { Container } from 'react-bootstrap';
import Image from 'next/image';
import leftArrow from '../../public/fi_arrow-left.svg';
import ceklis from '../../public/ceklis-icon.svg';
import three from '../../public/icon-3.svg';
import strip from '../../public/strip.svg';
import Link from 'next/link';

const TiketTop = () => {
    return (
        <>
            <Container className={style.tiketTopContainer}>
                <div className={style.tiketTopKiri}>
                    <div className={style.imageContainer}>
                        <Link href={'/search-result'}>
                            <Image src={leftArrow} alt='left-arrow' />
                        </Link>
                    </div>
                    <div className={style.tiketTopKiriDetail}>
                        <h6>Tiket</h6>
                        <p>Order ID: xxxxxxxx</p>
                    </div>
                </div>
                <div className={style.tiketTopKanan}>
                    <Image src={ceklis} alt='ceklis' />
                    <p>Pilih Metode</p>
                    <Image src={strip} alt='strip' />
                    <Image src={ceklis} alt='ceklis' />
                    <p>Bayar</p>
                    <Image src={strip} alt='strip' />
                    <Image src={three} alt='3' />
                    <p>Tiket</p>
                </div>
            </Container>
        </>
    )
};

export default TiketTop;