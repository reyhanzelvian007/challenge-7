import Footer from '../Footer/Footer';
import style from './BottomInvoice.module.css';
import TiketBottom from '../TiketBottom/TiketBottom';

const BottomInvoice = () => {
  return (
    <>
      <div className={style.bawahContainer}>
        <TiketBottom />
        <Footer />
      </div>
    </>
  )
};

export default BottomInvoice;
