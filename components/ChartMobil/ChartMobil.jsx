import style from './ChartMobil.module.css';
import { Chart } from 'chart.js/auto';
import { Pie } from 'react-chartjs-2';

const ChartMobil = () => {
    const dataChart = {
        labels: ['Innova', 'Avanza', 'Ferrari', 'Xenia', 'Kijang'],
        datasets: [
          {
            label: 'Mobil',
            data: [20, 50, 5, 50, 30],
            backgroundColor: [
              'rgb(255, 153, 0)',
              'rgb(255, 80, 80)',
              'rgb(0, 153, 51)',
              'rgb(51, 204, 204)',
              'rgb(51, 102, 255)'
            ]
          }
        ]
    };

    return (
        <>
          <div className={style.chartContainer}>
            <h2>Data Peminat Mobil</h2>
            <Pie data={dataChart} />
          </div>
        </>
    )
};

export default ChartMobil;