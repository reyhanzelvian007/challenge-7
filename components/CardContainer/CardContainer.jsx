import Card from "../Card/Card";
import style from './CardContainer.module.css';

const CardContainer = () => {
  return (
    <>
        <div className={style.cardContainer}>
            <Card />
        </div>
    </>
  )
}

export default CardContainer;