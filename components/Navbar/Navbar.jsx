import style from './Navbar.module.css';
import { Container } from 'react-bootstrap';

const Navbar = () => {
  return (
    <>
        <nav className={style.navStyle}>
            <Container className={style.navbar}>
                <div className={style.logo}></div>
                <div>
                    <ul className={style['navbar-nav']}>
                        <li className={style['nav-item']}>
                            <a className={style['nav-link']} href="#ourservices">Our Services</a>
                        </li>
                        <li className={style['nav-item']}>
                            <a className={style['nav-link']} href="#whyus">Why Us</a>
                        </li>
                        <li className={style['nav-item']}>
                            <a className={style['nav-link']} href="#testimonial">Testimonial</a>
                        </li>
                        <li className={style['nav-item']}>
                            <a className={style['nav-link']} href="#faq">FAQ</a>
                        </li>
                        <li className={style['nav-item']}>
                            <a className={style['nav-link']} href="#register"><p>Register</p></a>
                        </li>
                    </ul>
                </div>
            </Container>
        </nav>
    </>
  )
}

export default Navbar;