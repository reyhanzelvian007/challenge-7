import Navbar from '../Navbar/Navbar';
import style from './TopInvoice.module.css';
import TiketTop from '../TiketTop/TiketTop';

const TopInvoice = () => {
  return (
    <>
        <div className={style.topContainer}>
            <Navbar />
            <TiketTop />
        </div>
    </>
  )
}

export default TopInvoice;