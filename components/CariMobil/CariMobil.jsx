import style from '../CariMobil/CariMobil.module.css';
import Link from 'next/link';
import { dataCarAction } from "../../config/redux/actions/dataCarAction";
import { useDispatch } from "react-redux";

const CariMobil = () => {
    const dispatch = useDispatch();

    dispatch(dataCarAction());

    return (
        <>
            <div className={style.cariMobilContainer}>
                <div className={style.cariMobil}>
                    <div className={style.poinCariMobil}>
                        <div>
                            <p>Tipe Driver</p>
                        </div>
                        <div className={style.tipeDriver}>
                            <select name="tipeDriver" id="tipeDriver" placeholder="Pilih Tipe Driver">
                                <option value="sopir">Dengan Sopir</option>
                                <option value="tanpa-sopir">Tanpa Sopir (Lepas Kunci)</option>
                            </select>
                        </div>
                    </div>
                    <div className={style.poinCariMobil}>
                        <div>
                            <p>Tanggal</p>
                        </div>
                        <div className={style.date}>
                            <input className={style.input} id="date" type="date"/>
                        </div>
                    </div>
                    <div className={style.poinCariMobil}>
                        <div>
                            <p>Waktu Jemput/Ambil</p>
                        </div>
                        <div className={style.date}>
                            <input className={style.input} type="time" />
                        </div>
                    </div>
                    <div className={style.poinCariMobil}>
                        <div>
                            <p>Jumlah Penumpang (optional)</p>
                        </div>
                        <div className={style.penumpang}>
                            <input className={style.input} placeholder="Jumlah Penumpang" type="number" />
                        </div>
                    </div>
                    <div>
                        <Link href="/search-result">
                            <button className={style.buttonCariMobil}>
                                Cari Mobil
                            </button>
                        </Link>
                    </div>
                </div>
            </div>
        </>
    )
}

export default CariMobil;