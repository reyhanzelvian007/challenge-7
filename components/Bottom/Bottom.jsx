import React from 'react';
import style from '../Bottom/Bottom.module.css'
import CariMobil from '../CariMobil/CariMobil';
import Footer from '../Footer/Footer';
import ChartMobil from '../ChartMobil/ChartMobil';

const Bottom = () => {
  return (
      <>
        <div className={style.bottomContainer}>
            <CariMobil />
            <ChartMobil />
            <Footer />
        </div>
      </>
  )
}

export default Bottom;