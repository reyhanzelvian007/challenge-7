import style from './Hero.module.css';
import Navbar from "../Navbar/Navbar";
import image from "../../public/img_car.png";
import Image from 'next/image';

const Hero = () => {
    return (
        <div className={style.heroContainer}>
            <div className={style.container}>
                <Navbar />
                <div className={style.Hero}>
                    <div>
                        <h1>Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</h1>
                        <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                    </div>
                    <div>
                        <Image
                            src={image}
                            alt="car-image"
                            layout='raw'
                        />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Hero;